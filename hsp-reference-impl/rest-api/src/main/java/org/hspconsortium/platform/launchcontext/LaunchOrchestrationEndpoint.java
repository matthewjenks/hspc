/*
 * #%L
 * hsp-api
 * %%
 * Copyright (C) 2014 - 2015 Healthcare Services Platform Consortium
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

package org.hspconsortium.platform.launchcontext;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class LaunchOrchestrationEndpoint {

    private String authorizationServerLaunchEndpointURL;

    @javax.annotation.Resource(name="authorizationServerLaunchEndpointURL")
    public void setEndpointURL(String authorizationServerLaunchEndpoint) {
        this.authorizationServerLaunchEndpointURL = authorizationServerLaunchEndpoint;
    }

    @RequestMapping(value = "/smart/Launch", method = RequestMethod.POST)
    public void handleLaunchRequest(HttpServletRequest request, HttpServletResponse response) {
        response.setStatus(HttpServletResponse.SC_TEMPORARY_REDIRECT);
        response.setHeader("Location", this.authorizationServerLaunchEndpointURL);

    }

}
