/*
 * #%L
 * hsp-authorization-server
 * %%
 * Copyright (C) 2014 - 2015 Healthcare Services Platform Consortium
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

package org.hspconsortium.platform.authorization.service;

import com.nimbusds.jwt.JWT;
import org.apache.commons.lang.StringUtils;
import org.hspconsortium.platform.authorization.launchcontext.LaunchContextHolder;
import org.hspconsortium.platform.security.LaunchContext;
import org.mitre.oauth2.model.AuthenticationHolderEntity;
import org.mitre.oauth2.model.ClientDetailsEntity;
import org.mitre.oauth2.model.OAuth2AccessTokenEntity;
import org.mitre.oauth2.model.OAuth2RefreshTokenEntity;
import org.mitre.oauth2.service.impl.DefaultOAuth2ProviderTokenService;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.oauth2.common.OAuth2RefreshToken;
import org.springframework.security.oauth2.common.exceptions.InvalidClientException;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2Request;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.Date;
import java.util.Map;
import java.util.Set;

@Service("hspOAuth2ProviderTokenService")
public class HSPOAuth2ProviderTokenService extends DefaultOAuth2ProviderTokenService {

    private static final String PATIENT_ID_FIELD_NAME = "patient";

    @Override
    public OAuth2AccessTokenEntity createAccessToken(OAuth2Authentication authentication) throws AuthenticationException, InvalidClientException {
        OAuth2AccessTokenEntity token = super.createAccessToken(authentication);
        OAuth2Request authRequest = authentication.getOAuth2Request();
        LaunchContext launchContext = getLaunchContext(authRequest);
        return new HSPOAuth2AccessTokenEntity(token, launchContext);
    }

    private LaunchContext getLaunchContext(OAuth2Request authRequest){
        String LAUNCH_SCOPE = "launch";
        Map<String, String> requestParams = authRequest.getRequestParameters();
        //Whitespace separated list of scopes
        String requestedScopeParam = requestParams.get("scope");
        String[] scopes = StringUtils.isNotBlank(requestedScopeParam) ? requestedScopeParam.split(" ") : new String[0];

        LaunchContext launchContext = null;
        for(String nextScope : scopes){
            if(nextScope.startsWith(LAUNCH_SCOPE)){
                String[] launchScopeParts = nextScope.split(":");
                if(launchScopeParts.length ==2){
                    String launchId = launchScopeParts[1];
                    launchContext = LaunchContextHolder.getLaunchContext(launchId);
                }
                break;
            }
        }
        return launchContext;
    }

    public static final class HSPOAuth2AccessTokenEntity extends OAuth2AccessTokenEntity {

        OAuth2AccessTokenEntity rootToken;
        LaunchContext launchContext;

        public HSPOAuth2AccessTokenEntity(OAuth2AccessTokenEntity rootToken, LaunchContext launchContext) {
            this.rootToken = rootToken;
            this.launchContext = launchContext;
        }

        @Override
        public Long getId() {
            return this.rootToken.getId();
        }

        @Override
        public void setId(Long id) {
            this.rootToken.setId(id);
        }

        @Override
        public Map<String, Object> getAdditionalInformation() {
            Map<String, Object> additionalInfoMap = this.rootToken.getAdditionalInformation();
            if(launchContext != null){
                additionalInfoMap.put(PATIENT_ID_FIELD_NAME, launchContext.getPatientId());
            }

            return additionalInfoMap;
        }

        @Override
        public AuthenticationHolderEntity getAuthenticationHolder() {
            return this.rootToken.getAuthenticationHolder();
        }

        @Override
        public void setAuthenticationHolder(AuthenticationHolderEntity authenticationHolder) {
            this.rootToken.setAuthenticationHolder(authenticationHolder);
        }

        @Override
        public ClientDetailsEntity getClient() {
            return this.rootToken.getClient();
        }

        @Override
        public void setClient(ClientDetailsEntity client) {
            this.rootToken.setClient(client);
        }

        @Override
        public String getValue() {
            return this.rootToken.getValue();
        }

        @Override
        public void setValue(String value) throws ParseException {
            this.rootToken.setValue(value);
        }

        @Override
        public Date getExpiration() {
            return this.rootToken.getExpiration();
        }

        @Override
        public void setExpiration(Date expiration) {
            this.rootToken.setExpiration(expiration);
        }

        @Override
        public String getTokenType() {
            return this.rootToken.getTokenType();
        }

        @Override
        public void setTokenType(String tokenType) {
            this.rootToken.setTokenType(tokenType);
        }

        @Override
        public OAuth2RefreshTokenEntity getRefreshToken() {
            return this.rootToken.getRefreshToken();
        }

        @Override
        public void setRefreshToken(OAuth2RefreshTokenEntity refreshToken) {
            this.rootToken.setRefreshToken(refreshToken);
        }

        @Override
        public void setRefreshToken(OAuth2RefreshToken refreshToken) {
            this.rootToken.setRefreshToken(refreshToken);
        }

        @Override
        public Set<String> getScope() {
            return this.rootToken.getScope();
        }

        @Override
        public void setScope(Set<String> scope) {
            this.rootToken.setScope(scope);
        }

        @Override
        public boolean isExpired() {
            return this.rootToken.isExpired();
        }

        @Override
        public OAuth2AccessTokenEntity getIdToken() {
            return this.rootToken.getIdToken();
        }

        @Override
        public void setIdToken(OAuth2AccessTokenEntity idToken) {
            this.rootToken.setIdToken(idToken);
        }

        @Override
        public String getIdTokenString() {
            return this.rootToken.getIdTokenString();
        }

        @Override
        public JWT getJwt() {
            return this.rootToken.getJwt();
        }

        @Override
        public void setJwt(JWT jwt) {
            this.rootToken.setJwt(jwt);
        }

        @Override
        public int getExpiresIn() {
            return this.rootToken.getExpiresIn();
        }
    }
}
