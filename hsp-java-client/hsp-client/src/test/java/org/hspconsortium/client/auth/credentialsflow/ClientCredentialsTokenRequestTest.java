/*
 * #%L
 * hsp-client
 * %%
 * Copyright (C) 2014 - 2015 Healthcare Services Platform Consortium
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

package org.hspconsortium.client.auth.credentialsflow;

import org.apache.commons.lang.StringUtils;
import org.hspconsortium.client.auth.Scopes;
import org.hspconsortium.client.auth.SimpleScope;
import org.hspconsortium.client.auth.access.AccessToken;
import org.hspconsortium.client.auth.access.AccessTokenProvider;
import org.hspconsortium.client.auth.access.JsonAccessTokenProvider;
import org.hspconsortium.client.auth.credentials.ClientSecretCredentials;
import org.hspconsortium.client.session.clientcredentials.ClientCredentialsAccessTokenRequest;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

@Ignore
public class ClientCredentialsTokenRequestTest {

    @Test
    public void testClientCredentialsAccessTokenRequest() {
        Scopes requestedScopes = new Scopes();
        requestedScopes
                .add(new SimpleScope("launch"))
                .add(new SimpleScope("patient/*.read"));

        ClientSecretCredentials clientSecretCredentials = new ClientSecretCredentials("secret");
        ClientCredentialsAccessTokenRequest tokenRequest = new ClientCredentialsAccessTokenRequest("test_client", clientSecretCredentials, requestedScopes);
        AccessTokenProvider tokenProvider = new JsonAccessTokenProvider();
        AccessToken accessToken = tokenProvider.getAccessToken("http://localhost:8080/hsp-auth/token", tokenRequest);
        Assert.assertNotNull(accessToken);
        Assert.assertTrue(StringUtils.isNotBlank(accessToken.getValue()));
    }

//Amy's working example
//    String patientId = "ID9995679";
//    IGenericClient fhirClient = FhirClientFactory.createCredentialsFlowFhirClient("http://localhost:8080/hsp-api/data", "test_client", "secret", requestedScopes);
//    Patient patient = fhirClient.read().resource(Patient.class).withId(patientId).execute();
//    System.out.println(StringUtils.join(patient.getName().get(0).getGiven(), " ") + " " + patient.getName().get(0).getFamily().get(0));
//
//    Bundle results = fhirClient.search().forResource(Observation.class).where(
//            Observation.SUBJECT.hasId(patientId)).
//            and(Observation.CODE.exactly().identifier("8302-2")).execute();
//
//    List<BundleEntry> entries = results.getEntries();
//    for (BundleEntry entry : entries) {
//        System.out.println(((DateTimeDt)((Observation)entry.getResource()).getApplies()).getValueAsString() + " " +
//                ((QuantityDt)((Observation)entry.getResource()).getValue()).getValue());
//    }
}
