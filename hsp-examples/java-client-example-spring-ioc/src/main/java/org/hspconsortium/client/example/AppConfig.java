/*
 * #%L
 * hsp-client-example
 * %%
 * Copyright (C) 2014 - 2015 Healthcare Services Platform Consortium
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package org.hspconsortium.client.example;

import org.hspconsortium.client.auth.access.AccessTokenProvider;
import org.hspconsortium.client.auth.access.JsonAccessTokenProvider;
import org.hspconsortium.client.controller.FhirEndpointsProvider;
import org.hspconsortium.client.auth.Scopes;
import org.hspconsortium.client.auth.SimpleScope;
import org.hspconsortium.client.auth.credentials.ClientSecretCredentials;
import org.hspconsortium.client.session.clientcredentials.ClientCredentialsSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import javax.inject.Inject;

@Configuration
@PropertySource("classpath:config/config.properties")
public class AppConfig {

    @Autowired
    Environment env;

    @Bean
    public String fhirServicesUrl() {
        return env.getProperty("example.fhirServicesUrl");
    }

    @Bean
    public String clientId() {
        return env.getProperty("example.clientId");
    }

    @Bean
    public String scope() {
        return env.getProperty("example.scopes");
    }

    @Bean
    public String clientSecret() {
        return env.getProperty("example.clientSecret");
    }

    @Bean
    @Inject
    public ClientSecretCredentials clientSecretCredentials(String clientSecret) {
        return new ClientSecretCredentials(clientSecret);
    }

    @Bean
    public AccessTokenProvider tokenProvider() {
        return new JsonAccessTokenProvider();
    }

    @Bean
    public FhirEndpointsProvider fhirEndpointsProvider() {
        return new FhirEndpointsProvider.Impl();
    }

    @Bean
    @Inject
    // simulate two EHR by having two instances of session factory
    public ClientCredentialsSessionFactory<ClientSecretCredentials> ehr1SessionFactory(
            AccessTokenProvider tokenProvider, FhirEndpointsProvider fhirEndpointsProvider, String fhirServicesUrl,
            String clientId, ClientSecretCredentials clientSecretCredentials, String scope) {
        Scopes scopes = new Scopes();
        scopes.add(new SimpleScope(scope));
        return new ClientCredentialsSessionFactory<>(tokenProvider, fhirEndpointsProvider, fhirServicesUrl, clientId,
                clientSecretCredentials, scopes);
    }

    @Bean
    @Inject
    // simulate two EHR by having two instances of session factory
    public ClientCredentialsSessionFactory<ClientSecretCredentials> ehr2SessionFactory(
            AccessTokenProvider tokenProvider, FhirEndpointsProvider fhirEndpointsProvider, String fhirServicesUrl,
            String clientId, ClientSecretCredentials clientSecretCredentials, String scope) {
        Scopes scopes = new Scopes();
        scopes.add(new SimpleScope(scope));
        return new ClientCredentialsSessionFactory<>(tokenProvider, fhirEndpointsProvider, fhirServicesUrl, clientId,
                clientSecretCredentials, scopes);
    }
}
