/*
 * #%L
 * hsp-client-ioc-example
 * %%
 * Copyright (C) 2014 - 2015 Healthcare Services Platform Consortium
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package org.hspconsortium.client.example;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.*;

@Configuration
@ComponentScan
public class Application {

    public static void main(String[] args) {
        // create a Spring context
        System.out.println("Creating Spring Context...");
        ApplicationContext context =
                new AnnotationConfigApplicationContext(Application.class);

        String patientId = "ID9995679";

        // get the PatientCompareService
        System.out.println("Getting PatientCompareService...");
        PatientCompareService patientCompareService = context.getBean(PatientCompareService.class);

        int result = patientCompareService.comparePatientName(patientId);
        System.out.println("Comparison result: " + result);
//
//
//
//        // get the SessionFactory
//        ClientCredentialsSessionFactory<SystemAccessToken, SystemAccessTokenProvider, ClientSecretCredentials>
//                clientCredentialsSessionFactory = context.getBean(ClientCredentialsSessionFactory.class);
//
//        // create a session
//        Session<SystemAccessToken, SystemAccessTokenProvider> session = clientCredentialsSessionFactory.createSession();
//
//        // find an existing patient
//        String patientId = "ID9995679";
//        Patient patient = session.read().resource(Patient.class).withId(patientId).execute();
//        System.out.println("Found patient: "
//                + patient.getNameFirstRep().getGivenAsSingleString()
//                + " "
//                + patient.getNameFirstRep().getFamilyAsSingleString());
    }
}