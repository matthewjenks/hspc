/*
 * #%L
 * Healthcare Services Consortium Platform FHIR Server
 * %%
 * Copyright (C) 2014 - 2015 Healthcare Services Platform Consortium
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

package org.hspconsortium.platform.service.repository.impl;

import ca.uhn.fhir.model.dstu2.resource.Conformance;
import ca.uhn.fhir.model.primitive.StringDt;
import ca.uhn.fhir.model.primitive.UriDt;
import org.hspconsortium.platform.service.repository.MetadataRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ConfiguredMetadataRepository implements MetadataRepository {

    private String tokenEndpointExtensionUrl;
    private String tokenEndpointUri;
    private String authorizationEndpointExtensionUrl;
    private String authorizationEndpointUri;
    private String registrationEndpointExtensionUrl;
    private String registrationEndpointUri;

    public void setTokenEndpointExtensionUrl(String tokenEndpointExtensionUrl) {
        this.tokenEndpointExtensionUrl = tokenEndpointExtensionUrl;
    }

    public void setTokenEndpointUri(String tokenEndpointUri) {
        this.tokenEndpointUri = tokenEndpointUri;
    }

    public void setAuthorizationEndpointExtensionUrl(String authorizationEndpointExtensionUrl) {
        this.authorizationEndpointExtensionUrl = authorizationEndpointExtensionUrl;
    }

    public void setAuthorizationEndpointUri(String authorizationEndpointUri) {
        this.authorizationEndpointUri = authorizationEndpointUri;
    }

    public void setRegistrationEndpointExtensionUrl(String registrationEndpointExtensionUrl) {
        this.registrationEndpointExtensionUrl = registrationEndpointExtensionUrl;
    }

    public void setRegistrationEndpointUri(String registrationEndpointUri) {
        this.registrationEndpointUri = registrationEndpointUri;
    }

    public Conformance addConformance(Conformance conformance){

        List<Conformance.Rest> restList = conformance.getRest();
        Conformance.Rest rest = restList.get(0);
        Conformance.RestSecurity restSecurity = rest.getSecurity();

        restSecurity.addUndeclaredExtension(false, this.tokenEndpointExtensionUrl, new UriDt( this.tokenEndpointUri));
        restSecurity.addUndeclaredExtension(false, this.authorizationEndpointExtensionUrl, new UriDt( this.authorizationEndpointUri));
        restSecurity.addUndeclaredExtension(false, this.registrationEndpointExtensionUrl, new UriDt( this.registrationEndpointUri));

        return conformance;
    }

}
